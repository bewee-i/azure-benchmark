use warp::hyper::Response;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Request{}

pub fn handle (_: Request) -> impl warp::Reply {
    print!("Hello world!");
    Response::builder().body("")
}
