use std::env;
use std::net::Ipv4Addr;
use warp::Filter;
use serde::de::DeserializeOwned;

#[path = "../hello-world/src/mod.rs"]
mod hello_world;
#[path = "../read-rand/src/mod.rs"]
mod read_rand;
#[path = "../fib/src/mod.rs"]
mod fib;

#[tokio::main]
async fn main() {
    let api = handler("hello-world", hello_world::handle)
        .or(handler("read-rand", read_rand::handle))
        .or(handler("fib", fib::handle));

    let port_key = "FUNCTIONS_CUSTOMHANDLER_PORT";
    let port: u16 = match env::var(port_key) {
        Ok(val) => val.parse().expect("Custom Handler port is not a number!"),
        Err(_) => 3000,
    };

    warp::serve(api).run((Ipv4Addr::LOCALHOST, port)).await
}

fn handler<T: warp::Reply, D: 'static + DeserializeOwned + Send>(
    name: &'static str,
    callback: impl Fn(D)->T + Send + Clone
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Send + Clone {
    warp::get()
        .and(warp::path("api"))
        .and(warp::path(name))
        .and(warp::query().map(callback))
}
