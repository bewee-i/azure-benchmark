use warp::hyper::{Response, StatusCode};
use std::fs::File;
use std::io::Read;
use anyhow::Error;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Request {
    n: usize
}

pub fn handle(request: Request) -> impl warp::Reply {
    match read_rand(request.n) {
        Ok(_) => Response::builder().body(""),
        Err(err) => {
            eprintln!("An error occured: {:?}", err);
            Response::builder().status(StatusCode::INTERNAL_SERVER_ERROR).body("")
        },
    }
}

pub fn read_rand(n: usize) -> Result<(), Error> {
    let mut f = File::open("/dev/random")?;
    let mut buf = vec![0u8; n];
    f.read_exact(&mut buf)?;
    Ok(())
}
