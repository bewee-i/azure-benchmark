use serde::Deserialize;

#[derive(Deserialize)]
pub struct Request {
    n: u32
}

pub fn handle(request: Request) -> impl warp::Reply {
    fib(request.n).to_string()
}

fn fib(n: u32) -> u32 {
    if n <= 1 {
        n
    } else {
        fib(n - 1) + fib(n - 2)
    }
}
