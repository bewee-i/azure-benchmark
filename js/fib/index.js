module.exports = async function (context, req) {
    const n = +req.query.n;

    context.res = {
        body: fib(n)
    };
}

function fib(n) {
    return n <= 1 ? n : fib(n - 1) + fib(n - 2);
}
