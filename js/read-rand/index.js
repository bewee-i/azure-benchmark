module.exports = async function (context, req) {
    const fs = require('node:fs/promises');
    const { Buffer } = require('node:buffer');

    const n = +req.query.n;
    const file = await fs.open('/dev/random');
    const buf = Buffer.alloc(n);
    file.read({ buffer: buf, length: n });
    await file.close();

    context.res = {
        body: ""
    };
}
